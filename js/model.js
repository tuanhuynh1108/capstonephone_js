class SanPham {
  constructor(
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    type,
    desc,
    img,
    quantity
  ) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.type = type;
    this.desc = desc;
    this.img = img;
    this.quantity = quantity;
    this.tinhTien = function () {
      return this.quantity * this.price;
    };
  }
}
