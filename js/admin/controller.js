export function layThongTinTuForm() {
  let name = document.getElementById("phoneName").value;
  let price = document.getElementById("gia").value;
  let screen = document.getElementById("screen").value;
  let backCamera = document.getElementById("backCamera").value;
  let frontCamera = document.getElementById("frontCamera").value;
  let type = document.getElementById("select").value;
  let img = document.getElementById("imgLink").value;
  let desc = document.getElementById("moTa").value;
  let quantity = document.getElementById("quantity").value;
  return {
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    type,
    img,
    desc,
    quantity,
  };
}

export let renderDSSP = (DSSP) => {
  //   console.log("DSSP: ", DSSP);
  let contentHTML = "";
  DSSP.forEach((item) => {
    let contentTr = `
    <tr>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td><img src=${item.img} alt="#"  class="rounded mx-auto d-block"></td>
    <td>${item.desc}</td>
    <td onclick="xoaSp(${item.id})" class="btn btn-danger">Xóa</td>
    <td onclick="suaSp(${item.id})" class="btn btn-success">Sửa</td>
    </tr>
    
    `;
    contentHTML += contentTr;
  });

  document.getElementById("tbodyPhone").innerHTML = contentHTML;
};

export let showThongTinLenForm = (data) => {
  //   console.log("data: ", data);
  document.getElementById("phoneName").value = data.name;
  document.getElementById("gia").value = data.price;
  document.getElementById("screen").value = data.screen;
  document.getElementById("backCamera").value = data.backCamera;
  document.getElementById("frontCamera").value = data.frontCamera;
  document.getElementById("select").value = data.type;
  document.getElementById("imgLink").value = data.img;
  document.getElementById("moTa").value = data.desc;
  document.getElementById("quantity").value = 1;
};

export let resetForm = () => {
  document.getElementById("phoneForm").reset();
};

export let onMessage = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "left", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
