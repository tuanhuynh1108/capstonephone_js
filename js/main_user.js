const Base_URL = "https://63fec65ec5c800a72385839b.mockapi.io";
let DSSP = [];
function fetchSP() {
  batLoading();
  axios({
    url: `${Base_URL}/capstone`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      DSSP = res.data;
      let sp = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.type,
          item.desc,
          item.img,
          item.quantity
        );
      });

      render(sp);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

fetchSP(); // ra giao diện
renderSoLuong();
// let gioHang = [];

// fetchGioHang
function fetchGioHang() {
  batLoading();
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      gioHang = res.data;
      let SPGH = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.type,
          item.desc,
          item.img,
          item.quantity
        );
      });
      renderGioHang(SPGH);
      // renderSoLuong();
    })
    .catch((err) => {
      console.log(err);
    });
}
fetchGioHang();
// renderSoLuong();
function chonLoaiDt() {
  let chonDT = document.getElementById("chonDT").value;
  if (chonDT === "Apple") {
    batLoading();
    axios({
      url: `${Base_URL}/capstone`,
      method: "GET",
    })
      .then(function (res) {
        tatLoading();
        let sp = res.data.map((item) => {
          return new SanPham(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.type,
            item.desc,
            item.img
          );
        });
        let filter = sp.filter(function (item) {
          return item.type == "iphone" || item.type == "Iphone";
        });
        console.log("filter: ", filter);
        render(filter);
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  } else if (chonDT === "SamSung") {
    batLoading();
    axios({
      url: `${Base_URL}/capstone`,
      method: "GET",
    })
      .then(function (res) {
        tatLoading();
        let sp = res.data.map((item) => {
          return new SanPham(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.type,
            item.desc,
            item.img
          );
        });
        let filter = sp.filter(function (item) {
          return item.type == "Samsung";
        });
        console.log("filter: ", filter);
        render(filter);
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  } else {
    fetchSP();
  }
}

function themSP(id) {
  // call user = gio hang
  /**
   * khi nhấn nút:
   * Phải so sánh với biến gioHang (giống với mockAPI)
   *     + nếu chu7a có thì pót, fetch lại giỏ hàng
   *     + nếu có thì put vào , quantiti++ , fetch lại giỏ hàng
   */

  // renderSoLuong();
  batLoading();
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      let gioHang = res.data;
      toastify("Đã thêm sản phẩm vào giỏ hàng của bạn");

      console.log("gioHang: ", gioHang);
      if (gioHang.length == 0) {
        axios({
          url: `${Base_URL}/user`,
          method: "POST",
          data: DSSP[id - 1],
        })
          .then((res) => {
            // fetchGioHang();
            renderSoLuong();
          })
          .catch((err) => {
            console.log(err);
          });
      } else {
        // for (let i = 0; i < gioHang.length; i++) {
        //   if (DSSP[id - 1].name == gioHang[i].name) {
        //     gioHang[i].quantity++;
        //     axios({
        //       url: `${Base_URL}/user/${i + 1}`,
        //       method: "PUT",
        //       data: gioHang[i],
        //     })
        //       .then((res) => {
        //         console.log(res);
        //         fetchGioHang();
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });

        //   } else if (DSSP[id - 1].name != gioHang[i].name) {
        //     axios({
        //       url: `${Base_URL}/user`,
        //       method: "POST",
        //       data: DSSP[id - 1],
        //     })
        //       .then((res) => {
        //         console.log("res: ", res);
        //         fetchGioHang();
        //       })
        //       .catch((err) => {
        //         console.log(err);
        //       });
        //   }
        // }
        let spCanThem = gioHang.find((item) => {
          return item.name == DSSP[id - 1].name;
        });
        console.log("spCanThem: ", spCanThem);
        if (spCanThem) {
          spCanThem.quantity++;
          axios({
            url: `${Base_URL}/user/${spCanThem.id}`,
            method: "PUT",
            data: spCanThem,
          })
            .then((res) => {
              console.log(res); // chạy dòng này
              // fetchGioHang();
              renderSoLuong();
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          axios({
            url: `${Base_URL}/user`,
            method: "POST",
            data: DSSP[id - 1],
          })
            .then((res) => {
              console.log("res: ", res); // chạy dòng này
              // fetchGioHang();
              renderSoLuong();
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function xoaSPGioHang(id) {
  batLoading();
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      toastify("Xóa sản phẩm thành công");
      fetchGioHang();
      renderSoLuong();
      // renderSoLuong();
    })
    .catch((err) => {
      console.log(err);
    });
}

function tangSoLuong(id) {
  batLoading();
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "GET",
  })
    .then((res) => {
      let sp = res.data;
      sp.quantity++;
      axios({
        url: `${Base_URL}/user/${id}`,
        method: "PUT",
        data: sp,
      })
        .then((res) => {
          tatLoading();
          toastify("Đã tăng thêm 1 sản phẩm");
          fetchGioHang();
          renderSoLuong();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}

function giamSoLuong(id) {
  batLoading();
  axios({
    url: `${Base_URL}/user/${id}`,
    method: "GET",
  })
    .then((res) => {
      let sp = res.data;
      sp.quantity--;
      axios({
        url: `${Base_URL}/user/${id}`,
        method: "PUT",
        data: sp,
      })
        .then((res) => {
          tatLoading();
          toastify("Đã giảm 1 sản phẩm");
          fetchGioHang();
          renderSoLuong();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}
function gioHangUser() {
  document.getElementById("cart").style.display = "block";
  fetchGioHang();
}
let modal = document.getElementById("cart");
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
function closeCart() {
  document.querySelector("#cart").style.display = "none";
}
function thanhToan() {
  batLoading();
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then((res) => {
      let gioHangOnline = res.data;
      console.log("gioHang: ", gioHang);

      // function deleteGioHang(length) {
      //   if (length == 0) {
      //     return fetchGioHang(), renderSoLuong();
      //   } else {
      //     axios({
      //       url: `${Base_URL}/user/${length}`,
      //       method: "DELETE",
      //     })
      //       .then((res) => {
      //         deleteGioHang(length - 1);
      //         console.log(res);
      //       })
      //       .catch((err) => {
      //         console.log(err);
      //       });
      //   }
      // }
      // deleteGioHang(gioHangOnline.length);

      let promise = gioHangOnline.map((item) => {
        return axios({
          url: `${Base_URL}/user/${item.id}`,
          method: "DELETE",
        })
          .then((res) => {
            console.log(res);
          })
          .catch((err) => {
            console.log(err);
          });
      });
      console.log(promise);
      Promise.all(promise)
        .then((res) => {
          tatLoading();
          console.log(res);
          fetchGioHang();
          renderSoLuong();
        })
        .catch((err) => {
          console.log(err);
        });

      // fetchGioHang();
      toastify("Vui lòng thanh toán");
    })

    .catch((err) => {
      console.log(err);
    });
}
