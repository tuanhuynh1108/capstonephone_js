import {
  layThongTinTuForm,
  renderDSSP,
  showThongTinLenForm,
  resetForm,
  onMessage,
} from "./admin/controller.js";

const BASE_URL = "https://63fec65ec5c800a72385839b.mockapi.io";
let fetchDSSP = () => {
  axios({
    url: `${BASE_URL}/capstone`,
    method: "GET",
  })
    .then((res) => {
      //   console.log("res", res.data);
      renderDSSP(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchDSSP();

let xoaSp = (id) => {
  console.log("id: ", id);
  axios({
    url: `${BASE_URL}/capstone/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      //   console.log(res);
      onMessage("Xóa Thành Công");

      fetchDSSP();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaSp = xoaSp;

let IDspSua = -1;

let suaSp = (id) => {
  IDspSua = id;
  axios({
    url: `${BASE_URL}/capstone/${id}`,
    method: "GET",
  })
    .then((res) => {
      // console.log(res);
      $("#exampleModal").modal("show");

      showThongTinLenForm(res.data);
      // onMessage("Sửa Thành Công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.suaSp = suaSp;

let themSp = () => {
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/capstone`,
    method: "POST",
    data: data,
  })
    .then((res) => {
      // console.log(res);
      resetForm();
      $("#exampleModal").modal("hide");

      fetchDSSP();
      onMessage("Thêm Thành Công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.themSp = themSp;

let capNhat = () => {
  let data = layThongTinTuForm();
  console.log("data: ", data);
  axios({
    url: `${BASE_URL}/capstone/${IDspSua}`,

    method: "PUT",
    data,
  })
    .then((res) => {
      // console.log(res);

      resetForm();
      $("#exampleModal").modal("hide");
      fetchDSSP(res.data);

      onMessage("Cập Nhật Thành Công");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.capNhat = capNhat;
